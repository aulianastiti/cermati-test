package com.liauli.githubuserfinder.network.response

data class Response(
    val total_count: Int,
    val incomplete_results: Boolean,
    val items:  List<User>
)

data class User(val login: String, val id: Int, val avatar_url: String)