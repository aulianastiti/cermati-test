package com.liauli.githubuserfinder.network


internal object Router {

    internal const val BASE_URL = "https://api.github.com/"

    internal const val USER_SEARCH_URL = BASE_URL + "search/users"
}