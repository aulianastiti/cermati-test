package com.liauli.githubuserfinder.network

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class Request(private val disposable: Disposable) {

    private var subscriptions = CompositeDisposable()

    fun execute() {
        subscriptions.add(disposable)
    }

    fun unsubscribe() {
        subscriptions.dispose()
    }

}