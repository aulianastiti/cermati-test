package com.liauli.githubuserfinder

import android.content.Context


interface BasePresenter<T> {

    fun init(context: Context)
    /**
     * to refresh UUID and Token when resumed or needed
     */
    fun refreshRetrofit()

    /**
     * Binds presenter with a view when resumed. The Presenter will perform initialization here.
     *
     * @param view the view associated with this presenter
     */


    fun attachView(view: T)

    /**
     * Drops the reference to the view when destroyed
     */
    fun detachView()


}