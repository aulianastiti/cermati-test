package com.liauli.githubuserfinder.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.liauli.githubuserfinder.R
import com.liauli.githubuserfinder.network.response.User
import kotlinx.android.synthetic.main.item_users.view.*

class UserAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var usersList = mutableListOf<User>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return UserListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_users, parent, false))
    }

    override fun getItemCount(): Int = usersList.size
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as UserListViewHolder
        viewHolder.bindView(usersList[position])
    }

    fun addData(listOfUsers: List<User>) {
        if(usersList.isEmpty()){
            this.usersList = listOfUsers.toMutableList()
        }else {
            this.usersList.addAll(listOfUsers)
        }
        notifyDataSetChanged()
    }

    fun clearData(){
        usersList.clear()
        notifyDataSetChanged()
    }

    class UserListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(usersModel: User) {
            itemView.tv_username.text = usersModel.login
            Glide.with(itemView.context).load(usersModel.avatar_url).into(itemView.iv_icon)
        }
    }

}