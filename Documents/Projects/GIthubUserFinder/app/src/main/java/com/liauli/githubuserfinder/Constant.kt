package com.liauli.githubuserfinder

const val ITEM_PER_PAGE = 15

const val BAD_REQUEST = 400
const val UNAUTHORIZED = 401
const val FORBIDDEN = 403
const val NOT_FOUND = 404