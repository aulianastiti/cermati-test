package com.liauli.githubuserfinder.network.endpoint

import com.liauli.githubuserfinder.network.Router
import com.liauli.githubuserfinder.network.response.Response
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface GithubEndpoint {
    @GET(Router.USER_SEARCH_URL)
    abstract fun findUsers(@Query("q") query: String, @Query("page") page: Int, @Query("per_page") per_page: Int): Observable<Response>
}
