package com.liauli.githubuserfinder.network

import com.liauli.githubuserfinder.network.endpoint.GithubEndpoint
import com.liauli.githubuserfinder.network.response.Response
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import retrofit2.Retrofit


class Repository {

    private val service = Service()

    private lateinit var retrofit: Retrofit

    private lateinit var githubEndpoint: GithubEndpoint


    init {
        refreshRetrofit()
    }

    fun refreshRetrofit() {
        retrofit = service.createRetrofit()
        initEndpoints()
    }

    private fun initEndpoints() {
        githubEndpoint = retrofit.create(GithubEndpoint::class.java)
    }

    fun findUsers(query: String, page: Int, per_page: Int, findUserCallback: Callback.FindUserCallback): Request {
        return Request(
            githubEndpoint.findUsers(query, page, per_page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response: Response ->
                    findUserCallback.onSuccess(response)
                }, { t: Throwable? ->
                    val httpException = t as HttpException
                    findUserCallback.onFailed(httpException.code())
                })
        )
    }
}

object Callback {
    interface FindUserCallback {
        fun onSuccess(response: Response)
        fun onFailed(code: Int)
    }
}
