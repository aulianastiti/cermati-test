package com.liauli.githubuserfinder.ui

import android.content.Context
import android.widget.Toast
import com.liauli.githubuserfinder.FORBIDDEN
import com.liauli.githubuserfinder.ITEM_PER_PAGE
import com.liauli.githubuserfinder.R
import com.liauli.githubuserfinder.network.Callback
import com.liauli.githubuserfinder.network.Repository
import com.liauli.githubuserfinder.network.Request
import com.liauli.githubuserfinder.network.response.Response

class MainPresenter(private val repository: Repository) : MainContract.Presenter {
    private lateinit var mView: MainContract.View
    private lateinit var mContext: Context

    private var userRequest: Request? = null
    private lateinit var model: MainModel

    private var isLoading = false

    override fun init(context: Context) {
        mContext = context
        model = MainModel()
    }

    override fun attachView(view: MainContract.View) {
        mView = view
    }

    override fun getList(query: String) {
        mView.clearList()
        mView.showLoading()
        model.refreshPage()
        model.setQuery(query)
        search()
    }

    override fun getNext() {
        search()
    }

    private fun search() {
        if (model.allItemsLoaded() || isLoading) return
        isLoading = true
        model.getQuery()?.apply {
            userRequest =
                    repository.findUsers(this, model.getPage(), ITEM_PER_PAGE, object : Callback.FindUserCallback {
                        override fun onSuccess(response: Response) {
                            mView.hideLoading()
                            model.setIsReachLimit(response.incomplete_results)

                            if (response.incomplete_results) {
                                mView.showReachLimit()
                                return
                            }
                            if (response.items.isEmpty() && model.getPage() == 1) {
                                mView.showEmptyResultView()
                                return
                            }
                            if (response.items.size < ITEM_PER_PAGE) {
                                model.setAllItemsLoaded()
                            } else {
                                model.incrementPage()
                            }
                            mView.addItems(response.items)
                            isLoading = false
                        }

                        override fun onFailed(code: Int) {
                            when(code){
                                FORBIDDEN -> mView.showReachLimit()
                                else -> Toast.makeText(mContext, mContext.getString(R.string.failed), Toast.LENGTH_SHORT).show()
                            }
                            mView.hideLoading()
                            isLoading = false
                        }

                    })
        }

    }

    override fun refreshRetrofit() {
        repository.refreshRetrofit()
    }


    override fun detachView() {
        userRequest?.unsubscribe()
    }

}