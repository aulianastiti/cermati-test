package com.liauli.githubuserfinder.ui

import com.liauli.githubuserfinder.BasePresenter
import com.liauli.githubuserfinder.network.response.User

interface MainContract {
    interface View {
        fun showLoading()
        fun hideLoading()
        fun addItems(items: List<User>)
        fun clearList()
        fun showReachLimit()
        fun showEmptyResultView()
    }

    interface Presenter : BasePresenter<View> {
        fun getList(query: String)
        fun getNext()
    }

    interface Model {
        fun getPage(): Int
        fun setPage(offset: Int)
        fun incrementPage()
        fun refreshPage()
        fun getQuery(): String?
        fun setQuery(query: String)
        fun allItemsLoaded(): Boolean
        fun setAllItemsLoaded()
        fun setIsReachLimit(isReachLimit: Boolean)
        fun getIsReachLimit(): Boolean


    }
}