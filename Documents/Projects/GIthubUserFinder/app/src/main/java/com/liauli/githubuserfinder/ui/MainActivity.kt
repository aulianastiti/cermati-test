package com.liauli.githubuserfinder.ui

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.liauli.githubuserfinder.R
import com.liauli.githubuserfinder.adapter.UserAdapter
import com.liauli.githubuserfinder.hideKeyboard
import com.liauli.githubuserfinder.network.Repository
import com.liauli.githubuserfinder.network.response.User
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.appbar.*


class MainActivity : AppCompatActivity(), MainContract.View {

    private lateinit var presenter: MainPresenter

    private lateinit var userAdapter: UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenter(Repository())
        presenter.init(this)
        presenter.attachView(this)
        initList()
        initView()
    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView()
    }

    override fun showLoading() {
        pb_loading.visibility = VISIBLE
    }

    override fun hideLoading() {
        pb_loading.visibility = GONE
    }

    override fun addItems(items: List<User>) {
        userAdapter.addData(items)
    }

    override fun clearList() {
        userAdapter.clearData()
    }

    override fun showReachLimit() {
        Toast.makeText(this, getString(R.string.reached_limit), Toast.LENGTH_SHORT).show()
    }

    override fun showEmptyResultView() {
        rv_result.visibility = GONE
        tv_no_result.visibility = VISIBLE
    }

    private fun showResultsView(){
        rv_result.visibility = VISIBLE
        tv_no_result.visibility = GONE
    }

    private fun initList() {
        userAdapter = UserAdapter()
        rv_result.adapter = userAdapter
        rv_result.layoutManager = LinearLayoutManager(this)
        rv_result.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView
                    .layoutManager as LinearLayoutManager?
                linearLayoutManager?.itemCount?.apply {
                    if (this <= linearLayoutManager.findLastVisibleItemPosition() + 2) {
                        //signsAdapter.addData(getServiceData())
                        presenter.getNext()
                    }
                }

            }
        })
    }

    private fun initView(){
        et_search_bar.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                et_search_bar.hideKeyboard()
                showResultsView()
                presenter.getList(et_search_bar.text.toString())
                return@OnEditorActionListener true
            }
            false
        })
    }

}
