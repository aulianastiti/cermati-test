package com.liauli.githubuserfinder.ui

class MainModel : MainContract.Model {

    private var mQuery: String? = null

    private var mOffset: Int = 1

    private var allItemsLoaded = false

    private var mIsReachLimit = false

    override fun incrementPage() {
        mOffset += 1
    }

    override fun getPage(): Int {
        return mOffset
    }

    override fun setPage(offset: Int) {
        mOffset = offset
    }

    override fun refreshPage() {
        mOffset = 1
    }

    override fun getQuery(): String? {
        return mQuery
    }

    override fun setQuery(query: String) {
        mQuery = query
    }

    override fun allItemsLoaded(): Boolean {
        return allItemsLoaded
    }

    override fun setAllItemsLoaded() {
        allItemsLoaded = true
    }

    override fun setIsReachLimit(isReachLimit: Boolean) {
        mIsReachLimit = isReachLimit
    }

    override fun getIsReachLimit(): Boolean {
        return mIsReachLimit
    }


}